const jwt = require("jsonwebtoken");
const secret = "skcubrats";

module.exports.createAccessToken = (user) =>{

  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin
  }

  return jwt.sign(data,secret,{});
}

module.exports.verify = (req,res,next)=>{
  let token = req.headers.authorization;

  if (typeof token !== undefined){

    token = token.slice(7,token.length);

    return jwt.verify(token,secret,(err,data)=>{

      if (err){
        return res.send({auth:"failed"});
      }
    
      else{
        next();
      }

    })
  }
 
  else {
    return res.send({auth:"failed"});
  };
};

module.exports.decode = (token) => {
  if(typeof token !== "undefined"){

   
    token = token.slice(7, token.length);


    return jwt.verify(token, secret, (err, data) => {

      if (err) {

        return null;

      } else {
        return jwt.decode(token, {complete:true}).payload;
      };

    })

  } else {

    return null;

  };

};
/*
module.exports.verifyAdmin = (req, res, next) => {
  const token = req.headers.authorization?.split(" ")[1];

  if (!token) {
    return res.status(401).send({ auth: "failed", message: "No token provided" });
  }

  jwt.verify(token, secret, (err, decodedToken) => {
    if (err) {
      return res.status(401).send({ auth: "failed", message: "Failed to authenticate token" });
    }

    if (!decodedToken.isAdmin) {
      return res.status(403).send({ auth: "failed", message: "You are not authorized to perform this action" });
    }

    req.user = decodedToken;
    next();
  });
};
*/

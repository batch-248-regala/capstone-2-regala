const Order = require('../models/Order');

const User = require("../models/User");
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
// const { jwtSecret } = require('../config');

const Product = require("../models/Product");
// const auth = require('../auth');

// checkOutOrder - create a new order
module.exports.checkOutOrder = async (req, res) => {
  try {
    // Get user ID from JWT token
    const token = req.header('Authorization').replace('Bearer ', '');
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = decoded.id;

    // Validate user identity via JWT
    const user = await User.findById(userId);
    if (!user) {
      return res.status(401).send('Unauthorized');
    }

    // Generate a unique invoice number
    const invoiceNumber = crypto.randomBytes(16).toString('hex');

    // Extract order details from request body
    const orderDetails = req.body.orderDetails;

    // Extract product IDs and quantities from order details
    const products = orderDetails.map((item) => {
      return {
        productId: item.productId,
        quantity: item.quantity,
      };
    });

    // Calculate total amount
    let totalAmount = 0;
    for (let i = 0; i < orderDetails.length; i++) {
      totalAmount += orderDetails[i].quantity * orderDetails[i].subTotal;
    }

    // Create a new Order document with the extracted data
    const newOrder = new Order({
      userId: user._id,
      totalAmount: totalAmount,
      products: products,
    });

    // Save the new Order document to the database
    await newOrder.save();

    res.send(newOrder);
  } catch (error) {
    console.error(error);
    res.status(500).send('Server Error');
  }
};

// Retrieve User Details
module.exports.getUserDetails = async (req, res) => {
  try {
    // Validate the JWT and get the user id
    const userId = auth.verify(req);

    // Get the user details by id
    const user = await User.findById(userId);

    // Reassign the password to an empty string
    user.password = '';

    // Send the user details in the response
    res.json(user);
  } catch (err) {
    // Return an error message if the JWT validation fails
    res.status(401).json({ message: "Unauthorized access." });
  }
};

// Retrieve All Orders
module.exports.getAllOrders = (req, res) => {
  if (!req.user.isAdmin) {
    return res.status(401).send('Unauthorized');
  }

  Order.find({})
    .populate('orderedBy')
    .then((orders) => {
      res.send(orders);
    })
    .catch((err) => {
      res.status(500).send(err);
    });
};

// Retrieve Authenticated User's Orders
module.exports.getAuthUserOrders = async (req, res) => {
  try {
    const user = await User.findById(req.user.id);
    if (!user) {
      return res.status(404).send("User not found");
    }

    const orders = await Order.find({ orderedBy: user._id });
    res.send(orders);
  } catch (err) {
    res.status(500).send(err);
  }
};

// Set user as admin
module.exports.setAsAdmin = (req, res) => {
  // Validate JWT
  const token = req.headers.authorization.split(' ')[1];
  jwt.verify(token, jwtSecret, async (err, decoded) => {
    if (err) {
      res.status(401).send('Invalid token');
    } else {
      try {
        // Find user with matching ID
        const user = await User.findById(req.params.userId);
        if (!user) {
          res.status(404).send('User not found');
        } else {
          // Set user as admin
          user.isAdmin = true;
          await user.save();
          res.send('User updated successfully');
        }
      } catch (err) {
        res.status(500).send(err.message);
      }
    }
  });
};


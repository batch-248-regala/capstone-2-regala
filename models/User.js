// import Mongoose module
const mongoose = require('mongoose');

// create a new Mongoose schema for the 'User' model
	// to define the shape of the data that can be stored in the database for this model
let userSchema = new mongoose.Schema(
	// define several fields for the User model
		// such as 'userName', 'firstName', 'lastName', 'email', 'password', 'mobileNo', and 'isAdmin'
	{
		userName: {
			type: String,
			required: [true, "Username is required"]
		},

		firstName: {
			type: String,
			required: [true, "First Name is required"]
		},

		lastName: {
			type: String,
			required: [true, "Last Name is required"]
		},
		
		email: {
			type: String,
			required: false
		},

		password: {
			type: String,
			required: [true, "Password is required"]
		},

		mobileNo: {
			type: String,
			required: [true, "Mobile Number is required"]
		},

		isAdmin: {
			type: Boolean,
			default: false
		},
	}
);

// export the Mongoose model for the 'User' collection in the database
module.exports = mongoose.model("User", userSchema);

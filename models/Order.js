const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: [true, "User ID is requred."]    
  },
  products: [
    {
      productId: {
        type: String,
        required: [true, "This field is requred."]
      },price: {
            type: Number,
            default: 0
        },
        quantity: {
            type: Number,
            default: 0
        }
    }
  ],
  totalPrice: {
        type: Number,
        default: 0
    },
  totalQuantity: {
        type: Number,
        default: 0
    },
  status: {
        type: String,
        default: "to ship"
    },
  createdOn: {
        type: Date,
        default: new Date()
    }
});

const Order = mongoose.model('Order', orderSchema);

module.exports = Order;

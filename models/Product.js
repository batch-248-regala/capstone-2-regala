const mongoose = require('mongoose');

// Define the schema for a product
const productSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "Product Name is required"]
    },

    description: {
      type: String,
      required: [true, "Description is required"]
    },

    price: {
      type: Number,
      required: [true, "Price is required"]
    },

    isActive: {
      type: Boolean,
      default: true
    },

    createdOn: {
      type: Date,
      default: Date.now
    },

    orders: [
      {
        orderId: {
          type: mongoose.Schema.Types.ObjectId,
          ref: "Order"
        },
        quantity: {
          type: Number,
          required: [true, "Quantity is required"]
        }
      }
    ]
  }
);

// Export the Product model
module.exports = mongoose.model("Product", productSchema);

const express = require("express");
const router = express.Router();

const userController = require("../controllers/userControllers");

const auth = require("../auth.js");

router.post("/checkEmail", (req, res) => {
  userController.checkEmailExists(req.body).then((resultFromController) =>
    res.send(resultFromController)
  );
});

router.post("/register", (req, res) => {
  userController.registerUser(req.body).then((resultFromController) =>
    res.send(resultFromController)
  );
});

router.post("/login", userController.loginUser);

router.get("/authorization", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController
    .getProfile({ userId: userData.id })
    .then((resultFromController) => res.send(resultFromController));
});

router.get("/profile", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController
    .getAllUsers(userData.id)
    .then((resultFromController) => res.send(resultFromController));
});

router.get("/singleUser", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController
    .getSingleUser(userData.id)
    .then((resultFromController) => res.send(resultFromController));
});

module.exports = router;

/*
const express = require("express");
const router = express.Router();

const userController = require("../controllers/userControllers");

const auth = require("../auth.js");

router.post("/checkEmail", (req, res) => {
  userController.checkEmailExists(req.body).then((resultFromController) =>
    res.send(resultFromController)
  );
});

router.post("/register", (req, res) => {
  userController.registerUser(req.body).then((resultFromController) =>
    res.send(resultFromController)
  );
});

router.post("/login", userController.loginUser);

// This route can only be accessed by admin users
router.get("/users", auth.verify, auth.verifyAdmin, (req, res) => {
  userController.getAllUsers().then((resultFromController) =>
    res.send(resultFromController)
  );
});

router.get("/authorization", auth.verify, (req, res, next) => {
  const userData = auth.decode(req.headers.authorization);
  console.log(req.headers.authorization);
  userController
    .getProfile({ userId: userData.id })
    .then((resultFromController) => res.send(resultFromController))
    .catch(next);
});

module.exports = router;
*/

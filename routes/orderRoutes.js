const express = require('express');
const router = express.Router();
const orderControllers = require('../controllers/orderControllers');
const Controllers = require('../controllers/orderControllers');
const auth = require("../auth");

// // Create an order
// router.post("/createOrder", verify, orderControllers.checkOutOrder);

router.post("/createOrder", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  orderControllers
    .checkOutOrder(userData.id, req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// // Retrieve all orders
// router.get('/allOrders', verify, verifyAdmin, orderControllers.getAllOrders);

// // Retrieve orders made by the authenticated user
// router.get("/myOrders", verify, orderControllers.getAuthUserOrders);

// // Set user as admin
// router.put("/:userId/setAsAdmin", verify, verifyAdmin, orderControllers.setAsAdmin);

module.exports = router;
const express = require('express');
const router = express.Router();
const orderControllers = require('../controllers/orderControllers');
const auth = require("../auth");
const { verify, verifyAdmin } = auth;


// Create an order
router.post("/createOrder", verify, orderControllers.checkOutOrder);

// Retrieve user details
router.get("/getUserDetails", orderControllers.getUserDetails);

// Retrieve all orders
router.get('/allOrders', verify, verifyAdmin, orderControllers.getAllOrders);

// Retrieve orders made by the authenticated user
router.get("/myOrders", verify, orderControllers.getAuthUserOrders);

// Set user as admin
router.put("/:userId/setAsAdmin", verify, verifyAdmin, orderControllers.setAsAdmin);

module.exports = router;
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors'); // allows our backend to be available to our frontend application

const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const orderRoutes = require('./routes/orderRoutes');

const app = express();
const port = 3000;

// Connect to MongoDB Atlas database
mongoose.connect("mongodb+srv://admin:admin123@clusterbatch248.w2uuxss.mongodb.net/skcubrats-api?retryWrites=true&w=majority", {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

// Connection to database
let db = mongoose.connection;
db.on('error', console.error.bind(console, "Connection Error"));
db.once('open', () => console.log("Hi! We are connected to MongoDB Atlas!"));

// Middleware to parse JSON data
app.use(express.json());

// Allows all resources to access our backend application
app.use(cors());

// Middleware to parse JSON and URL-encoded data
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Routes for user, product, and order resources
app.use('/users', userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);

// Start the server and listen on port 3000
app.listen(port, () => console.log(`Server is running on localhost: ${port}`));
module.exports = app;

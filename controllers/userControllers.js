const bcrypt = require('bcrypt');

const User = require('../models/User');

const auth = require('../auth');

// User Registration
module.exports.registerUser = (reqBody) => {
  let newUser = new User({
  	userName: reqBody.userName,
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    email: reqBody.email,
    mobileNo: reqBody.mobileNo,
    password: bcrypt.hashSync(reqBody.password, 10),
  });
  return newUser.save().then((result, error) => {
    if (error) {
      return false;
    } else {
      return result;
    }
  });
};

// Check if email exists
module.exports.checkEmailExists = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(user =>
	{
		const message = "Email already exists";
		if (user){
			return message;
		}else{
			return false;
		}
	})
	.catch(err => console.log(err));
}

// Retrieve user details
module.exports.getProfile = (data) => {
  return User.findById(data.userId).then((result) => {
    result.isAdmin = true;
    return result;
  });
};

// Get all users
module.exports.getAllUsers = () => {

	return User.find({})
	.then(result => result)
	.catch(err => console.log(err));
};

// Get single user
module.exports.getSingleUser = (userId) => {

	return User.findById(userId)
	.then(result => result)
	.catch(err => console.log(err));
};

// User login
module.exports.loginUser = (req,res) => {
	User.findOne({email: req.body.email})
	.then(user => {

		if(user === null){
			return res.status(401).send('User does not exist');
		} 

		const isPasswordCorrect = bcrypt.compareSync(req.body.password, user.password)

		if(isPasswordCorrect){
			return res.send({accessToken: auth.createAccessToken(user)})
		} else {
			return res.status(401).send('Password is incorrect')
		}
		
	})
	.catch(err => console.log(err));
};



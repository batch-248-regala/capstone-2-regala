const Product = require('../models/Product');

// CREATE A PRODUCT
module.exports.createProduct = (req, res) => {
  // Check if product already exists in database
  Product.findOne({ name: req.body.name })
    .then(product => {
      if (product) {
        return res.status(409).send("Product already exists");
      } else {
        // If product does not already exist, create a new product and save it to database
        let newProduct = new Product({
          name: req.body.name,
          description: req.body.description,
          price: req.body.price,
        });
        newProduct.save()
          .then(savedProduct => {
            res.send(savedProduct);
          })
          .catch(err => {
            console.log(err);
            res.status(500).send("Error saving product");
          });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).send("Error finding product");
    });
}

// RETRIEVE ALL PRODUCTS
module.exports.getAllProducts = (req, res) => {
  Product.find({})
    .then(products => {
      res.send(products);
    })
    .catch(err => {
      console.log(err);
      res.status(500).send("Error finding products");
    });
};

// RETRIEVE ACTIVE PRODUCTS
module.exports.getActiveProducts = (req, res) => {
  Product.find({ isActive: true })
    .then(products => {
      res.json(products);
    })
    .catch(err => {
      console.log(err);
      res.status(500).send("Error finding active products");
    });
};

// RETRIEVE SINGLE PRODUCT 
module.exports.getSingleProduct = (req, res) => {
  Product.findById(req.params.id)
    .then(product => {
      if (!product) {
        return res.status(404).send("Product not found");
      }
      res.send(product);
    })
    .catch(err => {
      console.log(err);
      res.status(500).send("Error finding product");
    });
};

// UPDATE PRODUCT
module.exports.updateProduct = (req, res) => {
  let updates = {
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
  }

  Product.findByIdAndUpdate(req.params.id, updates, { new: true })
    .then(updatedProduct => {
      if (!updatedProduct) {
        return res.status(404).send("Product not found");
      }
      res.send(updatedProduct);
    })
    .catch(err => {
      console.log(err);
      res.status(500).send("Error updating product");
    });
};

// ARCHIVE A PRODUCT
module.exports.archiveProduct = (req, res) => {
  let updates = {
    isActive: false
  }

  Product.findByIdAndUpdate(req.params.id, updates, { new: true })
    .then(updatedProduct => {
      if (!updatedProduct) {
        return res.status(404).send("Product not found");
      }
      res.send(updatedProduct);
    })
    .catch(err => {
      console.log(err);
      res.status(500).send("Error archiving product");
    });
};

// ACTIVATE A PRODUCT
module.exports.activateProduct = async (req, res) => {
  try {
    const product = await Product.findByIdAndUpdate(req.params.id, { isActive: true }, { new: true });
    if (!product) {
      return res.status(404).json({ message: 'Product not found' });
    }
    res.json({ message: 'Product activated', product });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
};

// DELETE A PRODUCT 
module.exports.deleteProduct = (req, res) => {
  Product.findByIdAndRemove(req.params.id)
    .then(product => {
      if (!product) {
        return res.status(404).send("Product not found");
      }
      res.status(204).send();
    })
    .catch(err => {
      console.log(err);
      res.status(500).send("Error deleting product");
    });
};

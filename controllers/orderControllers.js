const Order = require('../models/Order');
const User = require("../models/User");
const jwt = require('jsonwebtoken');
const Product = require("../models/Product");
const { verify, decode, verifyAdmin } = require("../auth");

module.exports.checkOutOrder = async (userId, reqBody) => {
    
    try{

      let totalPrice = 0;
      let totalQuantity = 0;
       
          const orderedProducts = await Promise.all(reqBody.products.map(async(product) => {
              const productFromDb = await Product.findById(product.productId);
              console.log(productFromDb);
              
              const subTotalCurrentProduct = product.quantity * productFromDb.price;
              

              totalPrice += subTotalCurrentProduct;
              totalQuantity += product.quantity;

              const updatedProduct = await Product.findByIdAndUpdate(product.productId, {$push:{orders: {productId: product.productId, quantity: product.quantity }}}, {new: true});

              return {
                  productId: product.productId,
                  quantity: product.quantity,
                  price: subTotalCurrentProduct
              };
          }));
       
          const newOrder = new Order({
              userId,
              products: orderedProducts,
              totalPrice: totalPrice,
              totalQuantity
          });
           await newOrder.save();
           return {message: "Successfully created an order", newOrder};
        
    }catch(err){
      console.error(err);
      return "Error checkout";
    }
}


